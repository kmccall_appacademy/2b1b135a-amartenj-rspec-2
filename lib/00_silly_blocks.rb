def reverser(&proc)
  proc.call.split.map { |word| word.chars.reverse.join }.join(" ")
end

def adder(val = 1, &proc)
  val += proc.call
end

def repeater(n = 1, &proc)
  n.times { proc.call }
end
