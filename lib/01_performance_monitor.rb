def measure(n = 1, &proc)
  each_time = []

  n.times do
    start_time = Time.now
    proc.call
    each_time << Time.now - start_time
  end

  each_time.reduce(:+) / each_time.length
end
